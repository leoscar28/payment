<div class="col-md-3">
    <ul class="nav flex-column nav-pills">
        <li class="nav-item">
            <a class="nav-link {{ (request()->is('payment')) ? 'active' : '' }}" href="{{ route('payment') }}">
                <i class="fa fa-flag"></i> <span>{{ __('Payment history') }}</span>
            </a>
        </li>
        <li class="nav-item">
            <a  class="nav-link {{ (request()->is('payment/create')) ? 'active' : '' }}" href="{{ route('payment.create') }}">
                <i class="fa fa-bank"></i> <span>{{ __('Create new') }}</span>
            </a>
        </li>        
    </ul>
</div>