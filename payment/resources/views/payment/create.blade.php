@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @include('layouts.sidebar')
        
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">{{ __('New payment') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('payment.create') }}">
                        @csrf


                        <div class="form-group row">
                            <label for="amount" class="col-md-4 col-form-label text-md-right">{{ __('Amount') }}</label>

                            <div class="col-md-6">
                                <input id="amount" type="number"  step="0.00000001" class="form-control @error('amount') is-invalid @enderror" name="amount" value="{{ old('amount') }}" required autocomplete="amount" autofocus>

                                @error('amount')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="currency" class="col-md-4 col-form-label text-md-right">{{ __('Currency') }}</label>

                            <div class="col-md-6">
                                <select id="currency" name="currency" class="form-control  @error('currency') is-invalid @enderror" value="{{ old('currency') }}" required>
                                    <option value="KZT">KZT</option>
                                    <option value="RUB">RUB</option>
                                </select>
                                @error('currency')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="payer_phone" class="col-md-4 col-form-label text-md-right">{{ __('Payer Phone') }}</label>

                            <div class="col-md-6">
                                <input id="payer_phone" type="text" class="form-control @error('payer_phone') is-invalid @enderror" name="payer_phone" value="{{ old('payer_phone') }}" autocomplete="payer_phone">

                                @error('payer_phone')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="payer_email" class="col-md-4 col-form-label text-md-right">{{ __('Payer E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="payer_email" type="email" class="form-control @error('email') is-invalid @enderror" name="payer_email" value="{{ old('payer_email') }}" autocomplete="payer_email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-success">
                                    {{ __('Create') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection