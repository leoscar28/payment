@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @include('layouts.sidebar')
        
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">{{ __('Payment history') }}</div>

                <div class="card-body">
                    <table class="table table-sm">
                        <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Amount</th>
                                <th scope="col">Currency</th>
                                <th scope="col">Status</th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($payments as $payment)
                            <tr>
                                <th>{{ $payment->id }}</th>
                                <td>{{ $payment->amount }}</td>
                                <td>{{ $payment->currency }}</td>
                                <td><span class="badge @if($payment->status == -1) badge-danger @elseif ($payment->status == 0)  badge-info @else  badge-success @endif">{{ $status[$payment->status]}}</span></td>
                                <td class="text-center"><a class="btn btn-sm btn-success" href={{ url("/payment/view/{$payment->id}")}}>{{ __('View') }}</a></td>
                            </tr>
                            @endforeach
                        <tbody>
                    </table>

                    {{ $payments->render()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
