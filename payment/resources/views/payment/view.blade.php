@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @include('layouts.sidebar')
        
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">{{ __('View payment #').$waybill->id }}</div>

                <div class="card-body">
                   <table class="table table-sm">
                       <tbody>
                           @if ($waybill->amount)
                           <tr><td>{{ __('Amount')}}</td><td>{{ $waybill->amount }}</td></tr>
                           @endif
                           @if ($waybill->currency)
                           <tr><td>{{ __('Currency')}}</td><td>{{ $waybill->currency }}</td></tr>
                           @endif
                           @if ($waybill->payer_phone)
                           <tr><td>{{ __('Payer Phone')}}</td><td>{{ $waybill->payer_phone }}</td></tr>
                           @endif
                           @if ($waybill->payer_email)
                           <tr><td>{{ __('Payer Email')}}</td><td>{{ $waybill->payer_email }}</td></tr>
                           @endif
                           @if ($waybill->payer_card)
                           <tr><td>{{ __('Payer Card')}}</td><td>{{ $waybill->payer_card }}</td></tr>
                           @endif
                           @if ($waybill->status)
                           <tr><td>{{ __('Status')}}</td><td><span class="badge @if($waybill->status == -1) badge-danger @elseif ($waybill->status == 0)  badge-info @else  badge-success @endif">{{ $status }}</span></td></tr>
                           @endif
                           @if ($waybill->paid_on)
                           <tr><td>{{ __('Date')}}</td><td>{{ $waybill->paid_on }}</td></tr>
                           @endif
                       </tbody>
                   </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection