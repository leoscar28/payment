@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">        
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Waybill') }} №{{ $waybill->id }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if ($waybill->status <= 0)
                    <form method="POST" action="{{ route('waybill',['id' => $waybill->id]) }}">
                        @csrf

                    @endif
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">{{ __('Amount') }}</label>
                            <div class="col-md-6">
                                <div  class="form-control-plaintext">{{ $waybill->amount }}</div>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label  class="col-md-4 col-form-label text-md-right">{{ __('Currency') }}</label>
                            <div class="col-md-6">
                                <div class="form-control-plaintext">{{ $waybill->currency }}</div>                                    
                            </div>
                        </div>
                        
                        @if ($waybill->status <= 0 || ($waybill->status > 0 && $waybill->payer_phone))

                        <div class="form-group row">
                            <label for="payer_phone" class="col-md-4 col-form-label text-md-right">{{ __('Payer Phone') }}</label>

                            <div class="col-md-6">
                                @if($waybill->payer_phone || $waybill->status > 0) 
                                <div class="form-control-plaintext">{{ $waybill->payer_phone }}</div>
                                @else 
                                <input id="payer_phone" type="text" class="form-control  @error('payer_phone') is-invalid @enderror" name="payer_phone" value="{{ $waybill->payer_phone }}" autocomplete="payer_phone">
                                @endif

                                @error('payer_phone')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        @endif

                        @if ($waybill->status <= 0 || ($waybill->status > 0 && $waybill->payer_phone))

                        <div class="form-group row">
                            <label for="payer_email" class="col-md-4 col-form-label text-md-right">{{ __('Payer E-Mail Address') }}</label>
                           
                            <div class="col-md-6">
                                @if($waybill->payer_email || $waybill->status > 0)
                                <div class="form-control-plaintext">{{ $waybill->payer_email }}</div>
                                @else
                                <input id="payer_email" type="email" class="form-control  @error('email') is-invalid @enderror" name="payer_email" value="{{ $waybill->payer_email }}"  autocomplete="payer_email">

                                @endif
                                
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        @endif

                        @if ($waybill->status > 0)
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">{{ __('Status') }}</label>

                            <div class="col-md-6">
                                <div class="form-control-plaintext" ><span class="badge @if($waybill->status == -1) badge-danger @elseif ($waybill->status == 0)  badge-info @else  badge-success @endif">{{ $status }}</span></div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">{{ __('Date') }}</label>

                            <div class="col-md-6">
                                <div class="form-control-plaintext">{{ $waybill->paid_on }}</div>
                            </div>
                        </div>

                        @endif

                        @if ($waybill->status <= 0)
                        <div class="form-group row">
                            <label for="payer_card" class="col-md-4 col-form-label text-md-right">{{ __('Payer Card') }}</label>

                            <div class="col-md-6">
                                <input id="payer_card" type="text" class="form-control @error('payer_card') is-invalid @enderror" name="payer_card" value="{{ old('payer_card') }}" autocomplete="payer_card">

                                @error('payer_card')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="payer_card_exp" class="col-md-4 col-form-label text-md-right">{{ __('Payer Card Expires') }}</label>

                            <div class="col-md-6">
                                <input id="payer_card_exp" type="text" class="form-control @error('payer_card_exp') is-invalid @enderror" name="payer_card_exp" value="{{ old('payer_card_exp') }}" autocomplete="payer_card_exp">

                                @error('payer_card_exp')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-success">
                                    {{ __('Pay') }}
                                </button>
                            </div>
                        </div>
                    </form>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
