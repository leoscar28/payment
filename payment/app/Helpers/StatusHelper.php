<?php
namespace App\Helpers;
class StatusHelper{

    const STATUSES_DESCRIPTION = [
        '-1' => 'error',
        '0' => 'new',
        '1' => 'success',
        '2' => 'canceled'
    ];
}