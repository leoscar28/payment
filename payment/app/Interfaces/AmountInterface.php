<?php 

namespace App\Interfaces;

interface AmountInterface
{
    public function cutAmount(float $amount, int $len): float;
}