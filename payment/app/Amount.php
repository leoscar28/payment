<?php

namespace App;

use App\Interfaces\AmountInterface;
use Illuminate\Database\Eloquent\Model;

class Amount extends Model implements AmountInterface
{ 
    public function cutAmount(float $amount, int $len): float{

        $round = round($amount, $len);

        if ($round > $amount && $amount > 0) {
            $round -= pow(10,-$len); 
        } else if ($round < $amount && $amount < 0) {
            $round += pow(10,-$len);
        }

        return $round;
    }
}
