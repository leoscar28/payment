<?php

namespace App\Http\Controllers;

use App\Payment;
use App\Amount;
use App\Helpers\StatusHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PaymentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $payments = DB::table('payment')->paginate(10);

        return view('payment.index', ['payments' => $payments, 'status' => StatusHelper::STATUSES_DESCRIPTION]);
    }


    public function view(Request $request)
    {

        $waybill = Payment::where('id' , '=' , $request->id)->first();
        if ($waybill) {           
                  

            return view('payment.view',['waybill' => $waybill, 'status' => StatusHelper::STATUSES_DESCRIPTION[$waybill->status]]);
        }

        $request->session()->flash('status', 'Waybill not found!');
        return redirect('home');
    }


    public function create(Request $request)
    {
        if ($request->isMethod('post')) {
            
            $amount = new Amount();
            $payment = Payment::create([
                'amount' => $amount->cutAmount($request->amount, 2),
                'currency' => $request->currency,
                'payer_email' => $request->payer_email,
                'payer_phone' => $request->payer_phone
            ]);

            if ($payment) {
                $request->session()->flash('status', 'Payment was created! Payment URL = '.url("/waybill/{$payment->id}"));
                return redirect('home');
            }
            
        }
        return view('payment.create');
    }
    
}
