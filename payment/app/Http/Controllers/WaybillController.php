<?php

namespace App\Http\Controllers;

use App\Helpers\StatusHelper;
use Illuminate\Http\Request;
use App\Payment;
use App\Rules\CardExpires;
use App\Rules\CardNumber;

class WaybillController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index($id, Request $request)
    {
        $waybill = Payment::where('id' , '=' , $request->id)->first();
        if ($waybill) {
            
            if ($waybill->status <= 0 && $request->isMethod('post')) {
                $request->payer_phone ? $waybill->payer_phone = $request->payer_phone : false;
                $request->payer_email ? $waybill->payer_email = $request->payer_email : false;
                $waybill->status = -1;
                $waybill->save();
                $validation =  $this->validate($request, [                    
                    'payer_card_exp' => new CardExpires,
                    'payer_card' => new CardNumber
                ]);
                if ($validation) {
                    // for ($i = 8; $i < 16; $i++) {
                    //     $request->payer_card[$i] = '*';
                    // }
                    // $waybill->payer_card = $request->payer_card;
                    $waybill->payer_card = substr($request->payer_card,0, 7).'** **** '.substr($request->payer_card, -4);
                    $waybill->paid_on = date('Y-m-d H:i:s');
                    $waybill->status = 1;
                    $waybill->save();
                } 
                
            }           

            return view('waybill',['waybill' => $waybill, 'status' => StatusHelper::STATUSES_DESCRIPTION[$waybill->status]]);
        }
        $request->session()->flash('status', 'Waybill not found!');
        return redirect('/');
    }
}
