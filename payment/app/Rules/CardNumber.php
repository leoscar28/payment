<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class CardNumber implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if(strlen($value) != 19)  { 
            return false;
        } else {
            $value = preg_replace('/ /','', $value);

            $length = strlen($value);
            
            is_float($length/2) ? $s = 1 : $s = 0;
    
            $numbers = str_split($value);
    
            for ($i = $s; $i < $length; $i+=2) { 
                $int =  $numbers[$i]*2;           
                $numbers[$i] = $int > 9 ?  $int - 9 : $int;            
            }       
            
            return !is_float(array_sum($numbers)/10);
        }        
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Incorrect card number';
    }
}
