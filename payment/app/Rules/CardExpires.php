<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class CardExpires implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {

        if (strlen($value) != 5) { return false;}

        $value = explode('/', $value);
        $current = explode('/', date('m/y'));
        
        return !(($value[0] > 12) || ($value[0] <= 0) || ($value[1] < $current[1]) || (($value[1] == $current[1]) && ($value[0] < $current[0]))); 
        
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Incorrect expires date.';
    }
}
