<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table = 'payment';

    protected $fillable = [        
        'amount', 
        'currency', 
        'payer_email', 
        'payer_phone',
        'payer_card',
        'status',
        'created_at',
        'updated_at',
        'paid_on'
    ]; 
   
}
