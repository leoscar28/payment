<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/payment', 'PaymentController@index')->name('payment');
Route::get('/waybill/{id}', 'WaybillController@index')->name('waybill');
Route::post('/waybill/{id}', 'WaybillController@index')->name('waybill');
Route::get('/payment/create', 'PaymentController@create')->name('payment.create');
Route::post('/payment/create', 'PaymentController@create')->name('payment.create');
Route::get('/payment/view/{id}', 'PaymentController@view')->name('payment.view');